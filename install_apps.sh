#!/bin/zsh -i
set -e

source _utils.sh

function _install_applications() {
    log_main "### Installing additional apps & utils - Start ###"

    # load .env fle
    source_env

    if # guake
        ! command -v guake &>/dev/null && [[ $GUAKE_INSTALL == true ]]
    then
        log_main "guake not found. Installing..."
        sudo apt install -y guake
        # sudo cp /usr/share/applications/guake.desktop /etc/xdg/autostart/
    fi

    # nvm
    if ! command -v nvm &>/dev/null; then
        log_main "NVM not found. Installing..."
        curl -o- "https://raw.githubusercontent.com/nvm-sh/nvm/v${NVM_INSTALL_VERSION:=0.39.0}/install.sh" | bash
    fi


    log_main "### Installing additional apps & utils - DONE  ###"
}

"$@"

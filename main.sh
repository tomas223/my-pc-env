#!/bin/bash -i
# -i = shell is interactive

set -e # fail on error

source _utils.sh

function init_apps() {
    # sudo apt update
    ./install_apps.sh _install_applications
}

function init_zsh() {
    # sudo apt update
    ./install_zsh.sh _install_config_zsh
}

function init_all() {
    sudo apt update
    init_apps
    init_zsh
}

"$@"

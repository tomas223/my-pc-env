#!/bin/bash
set -e

function generate_public_ssh_key() {
    # if -f "${HOME}/.ssh/id_rsa" ; then
    _SSH_PATH="${HOME}/.ssh"
    _SSH_PRIV_PATH="${_SSH_PATH}/id_rsa"
    _SSH_PUBL_PATH="${_SSH_PATH}/id_rsa.pub"

    if [ ! -f $_SSH_PRIV_PATH ]; then
        log_err "Private key ${_SSH_PRIV_PATH} not found"
        exit 1
    fi

    log_main "Generating public key..."
    ssh-keygen -y -f $_SSH_PRIV_PATH >$_SSH_PUBL_PATH && log_main "Public key generated!"
    ls -al ${_SSH_PUBL_PATH}
}

"$@"

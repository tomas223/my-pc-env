#!/bin/bash -i
set -e

# COLORS

_DEF=$(tput sgr0) # default
_BLK=$(tput setaf 0)
_RED=$(tput setaf 1)
_GRN=$(tput setaf 2)
_YEL=$(tput setaf 3)
_BLU=$(tput setaf 4)
_MAG=$(tput setaf 5) # magenta
_CYA=$(tput setaf 6) # cyan
_WHI=$(tput setaf 7) # white

function log_main() {
    printf "${_CYA}=> $1${_DEF}\n"
}
function log_err() {
    printf "\n${_RED}=> $1${_DEF}\n"
}

function source_env() {

    if [ -f .env ]; then
        log_main ".env file found and loaded"
        source .env
    else
        log_main "'.env' file NOT found"
    fi
}

function confirm_yes() {
    printf "To continue type 'yes' and press Enter: "
    read answer
    if [ ! "$answer" == yes ]; then
        log_err "Please write explicitly 'yes' to confirm"
        exit 1
    fi
}

function generate_public_ssh_key() {
    # if -f "${HOME}/.ssh/id_rsa" ; then
    _SSH_PATH="${HOME}/.ssh"
    _SSH_PRIV_PATH="${_SSH_PATH}/id_rsa"
    _SSH_PUBL_PATH="${_SSH_PATH}/id_rsa.pub"

    if [ ! -f $_SSH_PRIV_PATH ]; then
        log_err "Private key ${_SSH_PRIV_PATH} not found"
        exit 1
    fi

    log_main "Generating public key..."
    ssh-keygen -y -f $_SSH_PRIV_PATH >$_SSH_PUBL_PATH && log_main "Public key generated!"
    ls -al ${_SSH_PUBL_PATH}
}

# "$@" - internal utils better not have this

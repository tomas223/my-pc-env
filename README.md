# Linux fresh install customization

Installing all stuff after reinstall of OS is pain. This should make it little bit less (at least for me)

- Recommended are Debian based OS - e.g. [Pop!\_OS by System76](https://pop.system76.com/)

## How to use

- First follow steps in [Before steps](#Before-steps) and [Installation](#Installation) instructions

- check for available commands in [.customrc](./customrc)
  - this file can be updated directly in this directory and `zsh` will automatically _source_ it on next usage
- check for available plugins in [.customrc](./customrc)
- use Guake with F12 (or what [you set-up](#After-steps-/-TODO))

## Before steps

Following steps are optional but reccommanded

- Install fonts for p10k from https://github.com/romkatv/powerlevel10k#fonts and set it in terminal(s) font `MesloLGS NF`
- Prepare installation env variables and change [defaults](./.env.example) as you need.
  Boolean values have to be in lowercase `true/false`

```bash
# copy .env example
cp .env.example .env
```

- Prepare .customrc file - for your custom aliases and function. For example check [.customrc.example](.customrc.example)

```bash
# copy .customrc example
cp .customrc.example .customrc
```

## Installation

First clone this repo to your home dir to `~/.my-pc-env`

```bash
git clone https://gitlab.com/tomas223/my-pc-env ~/.my-pc-env

cd ~/.my-pc-env

# install everything
make install_all
```

### Commands

```bash
# help
make

# all scripts can be run repeatedly
# but you are still using this on your own responsibility

# shortcut for install_zsh & install_apps
make install_all

# install/update zsh
# WARN: after first run ever you may need to 'log off' and back 'log on'
make install_zsh

# install/update apps
make install_apps

```

### How to use

## Ingredients

Included in automated scripts:

### make zsh

- zsh & [oh-my-zsh](https://ohmyz.sh/)
- preconfigured [Powerlevel10k themer](https://github.com/romkatv/powerlevel10k)
- git, curl, autojump
- mix of aliases and configs in [.customrc.example](.customrc.example)

### make apps

- [Guake](http://guake-project.org/) - top-down terminal for Gnome
- vim
- [nvm](https://github.com/nvm-sh/nvm) - node.js version manager
- fix for External Apple Keyboards (must be enabled in `.env` file)

## After steps / TODO

- Zsh should be default shell after logout+login. If not fix with following command

```bash
chsh -s $(which zsh)
```

- Guake:
  - `Toggle view` from `F12` to what you like (eg. `F1`)
  - Automatic start with OS in settings

### Configs & other nice to have stuff

- install [docker](https://docs.docker.com/engine/install/ubuntu/), [docker post-install steps](https://docs.docker.com/engine/install/linux-postinstall/), [docker-compose](https://docs.docker.com/compose/install/), vscode, chrome
- **vscode** - Terminal font Family: `MesloLGS NF`
- change computer hostname (if you want so it's more recognizable if are using more PCs)

```bash
# check
hostname
# change hostname
hostnamectl set-hostname pop-ryzen
```

- After installing `Dropbox(apt, Snap, ...)` make sure `Start on system start-up` is enabled
- [Dualboot Pop_OS! and Windows 10](https://github.com/spxak1/weywot/blob/main/Pop_OS_Dual_Boot.md) + [additional config](https://www.reddit.com/r/pop_os/comments/mme286/is_there_a_way_to_dual_boot_pop_os_and_windows_10/gtwtsix/?utm_source=reddit&utm_medium=web2x&context=3)
- [swap file](https://pop-planet.info/wiki/index.php?title=Swapfile#Swap_file) - how to create swap file
- checkout [Oh-My-Zsh plugins](https://github.com/ohmyzsh/ohmyzsh/wiki/Plugins) - interesting ones: `nvm`, `pip`, `yarn`, `command-not-found` `thefuck`

### Cool gnome extensions

- [IP Finder](https://extensions.gnome.org/extension/2983/ip-finder/) - shows public IP with country flag
- [Run Cat](https://extensions.gnome.org/extension/2986/runcat/) - cpu usage as a running cat
- [NetSpeed Gnome extension](https://extensions.gnome.org/extension/104/netspeed) - shows net speed (not compatible with latest Pop!\_Os)
- [Net speed simplified](https://extensions.gnome.org/extension/3724/net-speed-simplified/) - shows net speed (alternative)

### Apple External Keyboard settings/fixes

- [Enable FN keys by default](https://unix.stackexchange.com/a/156629)

### Limit battery charging on ASUS laptop to extend its' life

- Background: https://www.linuxuprising.com/2021/02/how-to-limit-battery-charging-set.html
- script: https://github.com/sreejithag/battery-charging-limiter-linux
- forked improved version: https://github.com/AnonymouX47/battery-charging-limiter-linux

```bash
mkdir -p $HOME/Applications
git clone https://github.com/AnonymouX47/battery-charging-limiter-linux \
   $HOME/Applications/battery-charging-limiter-linux
chmod +x $HOME/Applications/battery-charging-limiter-linux/limit*.sh
```

Then add to `~/.zshrc`:

```bash
# Limit battery charging for laptop
if [ -f $HOME/Applications/battery-charging-limiter-linux/limitd.sh ]; then
    alias charge_limit="$HOME/Applications/battery-charging-limiter-linux/limitd.sh"
fi
```

And use as per documentation on github. E.g.:

```bash
charge_limit 80
```

## How to develop

Open in VsCode using `Remote-Containers` extension

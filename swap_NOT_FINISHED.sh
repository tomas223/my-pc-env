#!/bin/bash
set -e # fail on error
# set -x # echo on

# Create & removeswap file
# inspiration: https://pop-planet.info/wiki/index.php?title=Swapfile
#
# Usage:
# $ sudo ./swap.sh create -s 16G
# $ sudo ./swap.sh remove

source _utils.sh

# defaults
SWAP_SIZE="4G"
SWAP_NAME="swapfile"

# parse scripts params
while getopts s: flag
do
    case "${flag}" in
        s) swapsize=${OPTARG};;
    esac
done


swap_size=${swapsize:-$SWAP_SIZE}
swap_filename=$SWAP_NAME

# !!!! Not working correctly yet
function create() {
    log_main "Creating swap '$swap_filename' with size: $swap_size"
    # As root use fallocate to create a swap file the size
    # of your choosing (M = Mebibytes, G = Gibibytes).
    fallocate -l 512M /swapfile
    # chown proxy:proxy install.env

    log_main "Haha"
    
    # Note: fallocate may cause problems with some file systems
    # such as F2FS  or XFS.[1] As an alternative,
    # using dd is more reliable, but slower:
    # dd if=/dev/zero of=/swapfile bs=1M count=512
    
    # Set the right permissions
    # (a world-readable swap file is a huge local vulnerability)
    chmod 600 /swapfile
    
    # After creating the correctly sized file, format it to swap:
    mkswap /swapfile
    
    # Activate the swap file:
    swapon /swapfile
    
    # Finally, edit fstab to add an entry for the swap file:
    # /etc/fstab
    # /swapfile none swap defaults 0 0
}



"$@"
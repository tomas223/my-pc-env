.PHONY: all help build run docker-build docker-run

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

install_zsh: ## install zsh, oh-my-zsh, .customrc and more
	@echo -n "Running: "
	./main.sh init_zsh

install_apps: ## install some apps & utils
	@echo -n "Running: "
	./main.sh init_apps

install_all: ## run both
	@echo -n "Running: "
	./main.sh init_all

generate_public_key: ## generate public key from $HOME/.ssh/id_rsa
	@echo -n "Running: "
	./_tools.sh generate_public_ssh_key

# test_source_bash_rcs: ## source current bash files. Good for testing
# 	# . .customrc
# 	echo $(cwd)

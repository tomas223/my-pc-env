#!/bin/bash -i
set -e

source _utils.sh

function _install_config_zsh() {
    log_main "### Installing preconfigured zsh, oh-my-zsh and more bash stuff - Start ###\n"
    # log_main "APT update"
    # sudo apt update

    # install dependencies in one of these formats "command command=package-name"
    local dependencies="git curl autojump pip=python3-pip zsh exa vim"
    log_main "Checking dependencies..."
    for dep in $dependencies; do
        # split string: https://stackoverflow.com/a/10520718
        local _command=${dep%=*}
        local _apt_lib=${dep#*=}
        if ! command -v $_command &>/dev/null; then
            echo "• ${_RED}$_command${_DEF} not found. Installing ${_YEL}$_apt_lib${_DEF}"
            sudo apt install -y $_apt_lib
        else
            echo "• ${_GRN}$_command${_DEF} already installed"
        fi
    done

    # Copy .zshrc
    log_main "Copying .zshrc etc. files"
    touch .customrc
    cp bash_configs/.zshrc \
        bash_configs/.p10k.zsh \
        ~/

    # => oh-my-zsh
    if [ ! -d ~/.oh-my-zsh ]; then
        log_main "oh-my-zsh not found. Installing..."
        export RUNZSH=no      # don't run zsh after install
        export KEEP_ZSHRC=yes # keep .zshrc from previous step
        sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
    else
        log_main "oh-my-zsh already installed."
    fi

    local ZSH_CUSTOM="${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}"

    # p10k theme
    local P10K_THEME_PATH="$ZSH_CUSTOM/themes/powerlevel10k"
    if [ ! -d $P10K_THEME_PATH ]; then
        log_main "p10k theme not found. Installing..."
        git clone --depth=1 https://github.com/romkatv/powerlevel10k.git $P10K_THEME_PATH
    fi

    # zsh-syntax-highlighting
    local HL_PLUGIN_PATH="$ZSH_CUSTOM/plugins/zsh-syntax-highlighting"
    if [ ! -d $HL_PLUGIN_PATH ]; then
        log_main "zsh-syntax-highlighting plugin not found. Installing..."
        git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $HL_PLUGIN_PATH
    fi

    # zsh-syntax-highlighting
    local SUGGESTIONS_PLUGIN_PATH="$ZSH_CUSTOM/plugins/zsh-autosuggestions"
    if [ ! -d $SUGGESTIONS_PLUGIN_PATH ]; then
        log_main "zsh-autosuggestions plugin not found. Installing..."
        git clone https://github.com/zsh-users/zsh-autosuggestions $SUGGESTIONS_PLUGIN_PATH
    fi

    # needed for colorize plugin
    if ! python3 -m pygments --help &>/dev/null; then
        log_main "python pygments not found (colorize plugin). Installing..."
        pip install Pygments
    fi

    log_main "### Installing ZSH - DONE ###\n"
    log_main "ZSH should be default shell after logout+login. If not fix with following command 'chsh -s $(which zsh)'"
}

"$@"
